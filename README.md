### Alexandre POITEVIN

20 allée Tourtonde  
47200 Marmande  
Tél (FR) : +33 66 63 24 06 36  
Mail :
[alexandre.poitevin](mailto:alexandre.poitevin@pm.me)[47@gmail.com](mailto:alexandre.poitevin@pm.me)  
[LinkedIn](https://gitlab.com/yahya-abou-imran) /
[GitLab](https://www.linkedin.com/in/alexandre-poitevin-yahya-abou-imran/)

Développeur Python Backend travaillant en méthodologie SCRUM.  
Capable de tirer des spécifications techniques à partir de besoins,
d’analyser des problèmes algorithmiques puis de les modéliser et de les
résoudre par un code de qualité et performant.  
Goût du travail en équipe, ouvert d’esprit, dynamique et réactif.

#### COMPÉTENCES PRINCIPALES

-   **Avancé**: Python, POO, TDD, Git, Principes SOLID, Shell, SCRUM
-   **Intermédiare**: SQL, Kivy, Flask

#### FORMATION

-   **2017** : Spécialisation Python de l’Université du Michigan
    ([coursera.org](https://www.coursera.org/account/accomplishments/specialization/746NYVF4PLYH))
-   **2017** : Analyste programmeur, titre pro de niveau III (CESI
    Alternance, Bordeaux)
-   **2015-2017** : Auto formation en Python, Git, Linux, algorithmes...
    ([openclassrooms.com](https://openclassrooms.com/fr))
-   **2007-2009** : Licence de mathématiques niveau 2ème année non
    validée (Bordeaux 1)
-   **2007** : Baccalauréat S SI spé mathématiques (Lycée Val de
    Garonne, Marmande)

#### EXPÉRIENCES PROFESSIONNELLES

-   **Juillet 2017** : Développeur stagiaire en intégration OpenERP/Odoo
    (Aquilog, Talence)

#### PROJETS OPEN SOURCE

-   **Librairies Python** :

    -   [**checktypes**](https://pypi.org/project/checktypes) : outils
        de data validation
    -   [**hybridset**](https://pypi.org/project/hybridset/)
        : structure de données optimisée pour les valeurs hashables et
        non hashables

#### COMPÉTENCES COMPLÉMETAIRES

-   **Éditeurs de textes et IDE**: Vim, PyCharm
-   **OS**: GNU/Linux (Debian et dérivées, Arch)
-   **Langues**: Français (natif), Anglais (bon niveau), Arabe
    littéraire (bon niveau)

